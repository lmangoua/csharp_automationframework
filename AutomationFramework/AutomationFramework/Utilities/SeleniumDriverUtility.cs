﻿using AutomationFramework.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Linq;
using System.Collections.ObjectModel;

/** 
 * @author: lmangoua
 *  Date: 20/08/2018
 *  
 */

namespace AutomationFramework
{
    public class SeleniumDriverUtility
    {
        public static IWebDriver Driver { get; set; }
        public static int WaitTimeout = 6;
        private readonly Enums.BrowserType browserType;
         
        #region StartDriver
        public static void StartDriver()
        {
            try
            {
                var options = new ChromeOptions();
                options.AddArguments("disable-infobars"); //To remove message "chrome is being controlled by automated test software"
                options.AddArguments("--start-maximized"); //To maximize browser
                Driver = new ChromeDriver(options);
                SeleniumDriverUtility.Wait(TimeSpan.FromSeconds(5)); 
                Console.WriteLine("\n**********************\n");
                Console.WriteLine("\n******Started the Driver successfully******\n");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to start the Driver ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Initialize driver to run on Sauce Labs (Cloud Service for continuous testing)
        #region Initialize
        public static void Initialize()
        {
            try
            {
                var options = new ChromeOptions();
                options.AddArguments("disable-infobars"); //To remove message "chrome is being controlled by automated test software"
                options.AddArguments("--start-maximized"); //To maximize browser 
                                                           //DesiredCapabilities capabilities = DesiredCapabilities.Firefox();
                                                           //DesiredCapabilities capabilities = DesiredCapabilities.Chrome();
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities = options.ToCapabilities() as DesiredCapabilities;
                capabilities.SetCapability(CapabilityType.Platform, "Windows 10");
                capabilities.SetCapability(CapabilityType.Version, "");
                capabilities.SetCapability("name", "Web App");
                capabilities.SetCapability("username", "lionel");
                capabilities.SetCapability("accessKey", "67dd30a2-705e-4532-8ffc-13f78c493fd8");

                Driver = new RemoteWebDriver(new Uri("https://ondemand.saucelabs.com:80/wd/hub"), capabilities);

                SeleniumDriverUtility.Wait(TimeSpan.FromSeconds(5));
                Console.WriteLine("******Initialized the browser successfully******");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to initialize the browser ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region Close
        public static void Close()
        {
            try
            {
                Driver.Close();
                Console.WriteLine("******END******");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to close the browser ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region Wait
        public static void Wait(TimeSpan timeSpan)
        {
            try
            {
                Thread.Sleep((int)(timeSpan.TotalSeconds * 1000));
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to wait ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region Pause
        public static void Pause(int millisecondsWait)
        {
            try
            {
                Thread.Sleep(millisecondsWait);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to pause ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Static methods
        #region WaitForElementByXpath
        public static void WaitForElementByXpath(string elementXpath)
        {
            bool elementFound = false;
            try
            {
                int WaitCount = 0;
                while (!elementFound && WaitCount < WaitTimeout)
                {
                    try
                    {
                        WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(2));

                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                        if (wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.VisibilityOfAllElementsLocatedBy(By.XPath(elementXpath))) != null)
                        {
                            elementFound = true;
                            Console.WriteLine("[INFO] Found element by Xpath: " + elementXpath);
                            break;
                        }
                    }
                    catch (Exception e)
                    {
                        elementFound = false;
                        Console.WriteLine("[ERROR] Did Not Find element by Xpath: " + elementXpath);
                    }

                    WaitCount++;
                }

                if (WaitCount == WaitTimeout)
                {
                    GetElementFound1(elementFound);
                    Console.WriteLine("[INFO] Reached TimeOut whilst waiting for element by Xpath: " + elementXpath);
                    Assert.Fail();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to wait for element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }

            GetElementFound(elementFound);
        }

        private static bool GetElementFound1(bool elementFound)
        {
            return elementFound;
        }

        private static bool GetElementFound(bool elementFound)
        {
            return elementFound;
        }
        #endregion

        #region EnterTextByXpath
        public static void EnterTextByXpath(string elementXpath, string textToEnter)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                elementToTypeIn.Clear();
                Thread.Sleep(200);
                elementToTypeIn.SendKeys(textToEnter);

                Console.WriteLine("[INFO] Entered text by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to enter text by Xpath  ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region EnterText2ByXpath
        public static void EnterText2ByXpath(string elementXpath, string textToEnter)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                elementToTypeIn.SendKeys(textToEnter);

                Console.WriteLine("[INFO] Entered text by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to enter text by Xpath  ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region HoverOverElementByXpath 
        public static void HoverOverElementByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);

                Actions hoverTo = new Actions(Driver);
                hoverTo.MoveToElement(Driver.FindElement(By.XPath(elementXpath)));
                hoverTo.Perform();

                Console.WriteLine("[INFO] Hovered over element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to hover over element Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region HoverOverElementAndClickSubElementXpath
        public static void HoverOverElementAndClickSubElementXpath(string elementXpath, string subElementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);

                Actions hoverTo = new Actions(Driver);
                var menuHoverLink = Driver.FindElement(By.XPath(elementXpath));
                hoverTo.MoveToElement(menuHoverLink);

                var subLink = menuHoverLink.FindElement(By.XPath(subElementXpath));
                hoverTo.MoveToElement(subLink);
                hoverTo.Click();
                hoverTo.Perform();
                Console.WriteLine("[INFO] Hovered over element and clicked sub element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to hover over element and click sub element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion               

        #region ClickElementByXpath
        public static void ClickElementByXpath(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Thread.Sleep(3000);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));

                try
                {
                    elementToClick.Click();
                    Console.WriteLine("[INFO] Attempted Second Click by Xpath: " + elementXpath, e.Message);
                }
                catch
                {
                    Console.WriteLine("[ERROR] Failed to click element by Xpath: " + elementXpath, e.Message);
                    Assert.Fail();
                }
            }
        }
        #endregion

        //Scroll To Element
        #region ScrollToElementByXpath
        public static void ScrollToElementByXpath(string elementXpath)
        {
            try
            {
                var element = Driver.FindElement(By.XPath(elementXpath));
                ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].scrollIntoView(true);", element);

                Console.WriteLine("[INFO] Scrolled To Element - " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Scroll To Element ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Scroll To Element 2
        #region ScrollToElement2ByXpath
        public static void ScrollToElement2ByXpath(string elementXpath)
        {
            WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(30));
            try
            {
                for (int i = 0; ; i++)
                {
                    if (i >= 30)
                    {
                        Actions scroll = new Actions(Driver);
                        scroll.KeyDown(Keys.Control).SendKeys(Keys.End);
                        wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                        Console.WriteLine("[INFO] Scrolled To Element - " + elementXpath);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Scroll To Element ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region DoubleClickElementByXpath
        public static void DoubleClickElementByXpath(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                //Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                elementToClick.Click();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[ERROR] Failed to click element by Xpath: " + elementXpath, e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region SendKeys
        public static void SendKeys(string choice)
        {
            try
            {
                Actions action = new Actions(Driver);

                switch (choice.ToUpper())
                {
                    case "ARROW DOWN":
                        {
                            action.SendKeys(Keys.ArrowDown);
                            action.Perform();
                            Console.WriteLine("[INFO] Pressed Key: \"" + choice + "\" successfully");
                            break;
                        }
                    case "COPY ALL":
                        {
                            action.SendKeys(Keys.Control + "a");
                            action.SendKeys(Keys.Control + "c");
                            action.Perform();
                            Console.WriteLine("[INFO] Pressed Key: \"" + choice + "\" successfully");
                            break;
                        }
                    case "ENTER":
                        {
                            action.SendKeys(Keys.Enter);
                            action.Perform();
                            Console.WriteLine("[INFO] Pressed Key: \"" + choice + "\" successfully");
                            break;
                        }
                    case "TAB":
                        {
                            action.SendKeys(Keys.Tab);
                            action.Perform();
                            Console.WriteLine("[INFO] Pressed Key: \"" + choice + "\" successfully");
                            break;
                        }
                    case "REFRESH":
                        {
                            action.SendKeys(Keys.F5);
                            action.Build();
                            action.Perform();
                            Driver.Navigate().Refresh();
                            Console.WriteLine("[INFO] Pressed Key: \"" + choice + "\" successfully");
                            break;
                        }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to send keypress: \"" + choice + "\" to element ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region RetrieveTextByXpath
        public string RetrieveTextByXpath(string elementXpath)
        {
            string retrievedText = "";
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath));
                retrievedText = elementToRead.Text; //.getText() in java
                Console.WriteLine("[INFO] Text: \"" + retrievedText + "\" retrieved successfully from element Xpath - " + elementXpath);

                return retrievedText;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to retrieve text from element Xpath ---\n{0}", e.Message);
                return retrievedText;
            }
        }
        #endregion

        #region ExtractTextByXpath
        public static string ExtractTextByXpath(string elementXpath)
        {
            string extractedText = "";
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath));
                extractedText = elementToRead.Text; //.getText() in java
                Console.WriteLine("[INFO] Text: \"" + extractedText + "\" extracted successfully from element Xpath - " + elementXpath);
                return extractedText;

            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to extract text from element Xpath ---\n{0}", e.Message);
                return extractedText;
            }
        }
        #endregion 

        #region SelectFromDropDownListUsingXpath
        public static void SelectFromDropDownListUsingXpath(string elementXpath, string valueToSelect)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var dropDownList = new SelectElement(Driver.FindElement(By.XPath(elementXpath)));
                var formXpath = Driver.FindElement(By.XPath(elementXpath));
                formXpath.Click();
                dropDownList.SelectByText(valueToSelect);

                Console.WriteLine("[INFO] Selected Text: \"" + valueToSelect + "\" from dropdown list by Xpath: " + elementXpath);
                formXpath.Click();
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to select text: \"" + valueToSelect + "\" from dropdown list by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region SelectFromDropdownListViaDivUsingXpath
        public static void SelectFromDropdownListViaDivUsingXpath(String dropdownlistXpath, String valueToSelect)
        {
            try
            {
                WaitForElementByXpath(dropdownlistXpath);
                var dropDownList = Driver.FindElement(By.XPath(dropdownlistXpath));
                dropDownList.Click();
                dropDownList = Driver.FindElement(By.XPath(valueToSelect));
                Actions select = new Actions(Driver);
                select.MoveToElement(dropDownList);
                select.Click(dropDownList);
                select.Perform();
                Console.WriteLine("[INFO] Selected Text: \"" + valueToSelect + "\" from dropdown list by Xpath ---" + dropdownlistXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to select text: " + valueToSelect + " from dropdown list by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //To create random strings
        #region RandomString
        public static string RandomString()
        {
            Random random = new Random();
            const string chars = "0123456789";
            return new string(Enumerable.Repeat(chars, 4)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion

        //Refresh page
        #region Refresh
        public static void Refresh()
        {
            Driver.Navigate().Refresh();
        }
        #endregion

        //Select All And Copy
        #region SelectAllAndCopyByXpath
        public static void SelectAllAndCopyByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                Actions action = new Actions(Driver);
                elementToTypeIn.Click();
                action.KeyDown(Keys.Control).SendKeys("a").KeyUp(Keys.Control).Perform(); //Ctrl+a
                action.KeyDown(Keys.Control).SendKeys("c").KeyUp(Keys.Control).Perform(); //Ctrl+c
                Console.WriteLine("[INFO] Selected All and Copied successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Select All and Copy ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion 

        //Paste Copied Text
        #region PasteCopiedText
        public static void PasteCopiedTextByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                Actions action = new Actions(Driver);
                elementToTypeIn.Click();
                elementToTypeIn.SendKeys(Keys.Control + "a"); //Ctrl+a
                elementToTypeIn.SendKeys(Keys.Delete); //Delete
                elementToTypeIn.SendKeys(Keys.Control + "v"); //Ctrl+v
                Console.WriteLine("[INFO] Pasted Copied Text successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Paste Copied Text ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Select All, Cut & Paste Text
        #region SelectAllCutAndPasteTextByXpath
        public static void SelectAllCutAndPasteTextByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                Actions action = new Actions(Driver);
                elementToTypeIn.Click();
                action.KeyDown(Keys.Control).SendKeys("a").KeyUp(Keys.Control).Perform(); //Ctrl+a
                action.KeyDown(Keys.Control).SendKeys("c").KeyUp(Keys.Control).Perform(); //Ctrl+c
                action.KeyDown(Keys.Control).SendKeys("x").KeyUp(Keys.Control).Perform(); //Ctrl+x
                action.KeyDown(Keys.Control).SendKeys("v").KeyUp(Keys.Control).Perform(); //Ctrl+v
                Console.WriteLine("[INFO] Selected All, Cut & Pasted Text successfully");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Select All, Cut & Paste Text ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Key Press
        #region KeyPress
        public static void KeyPress(string keyToPress)
        {
            try
            {
                Actions action = new Actions(Driver);
                action.SendKeys(keyToPress);
                action.Perform();
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed Pressing Key - " + keyToPress + " ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Validate Entered Text
        #region ValidateEnteredTextByXpath
        public static void ValidateEnteredTextByXpath(string elementXpath, string textToValidate)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath)).GetAttribute("value");
                Assert.IsTrue(elementToRead.Contains(textToValidate));
                Console.WriteLine("[INFO] Text: \"" + textToValidate + "\" validated successfully from element Xpath - " + elementXpath);
                //return textToValidate;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate text \"" + textToValidate + "\" from element Xpath ---\n{0}", e.Message);
                //return textToValidate;
                Assert.Fail();
            }
        }
        #endregion

        //Validate Selected Text
        #region ValidateSelectedTextByXpath
        public static void ValidateSelectedTextByXpath(string elementXpath, string textToValidate)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath)).Text;
                Assert.IsTrue(elementToRead.Contains(textToValidate));
                Console.WriteLine("[INFO] Text: \"" + textToValidate + "\" validated successfully from element Xpath - " + elementXpath);

                //return textToValidate;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate text \"" + textToValidate + "\" from element Xpath ---\n{0}", e.Message);
                //return textToValidate;
                Assert.Fail();
            }
        }
        #endregion

        //Validate Selected Checkboxes
        #region ValidateSelectedCheckboxByXpath
        public static void ValidateSelectedCheckboxByXpath(string elementXpath, string checkboxToValidate)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(2));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                var checkBox = Driver.FindElement(By.XPath(elementXpath));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));

                if (checkBox.Selected)
                {
                    Console.WriteLine("[INFO] Checkbox: \"" + checkboxToValidate + "\" is Checked and validated successfully from element Xpath - " + elementXpath);
                }
                else
                {
                    elementToClick.Click();
                    Console.WriteLine("[INFO] Checked: \"" + checkboxToValidate + "\" checkbox successfully from element Xpath - " + elementToClick);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate checkbox \"" + checkboxToValidate + "\" from element Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Validate Field Created
        #region ValidateFieldCreatedByXpath
        public static void ValidateFieldCreatedByXpath(string NameXpath, string WidthXpath, string RequiredXpath, string nameToValidate, string widthToValidate, string requiredToValidate)
        {
            try
            {
                WaitForElementByXpath(NameXpath);
                WaitForElementByXpath(WidthXpath);
                WaitForElementByXpath(RequiredXpath);
                //WaitForElementByXpath(nameToValidate);
                //WaitForElementByXpath(widthToValidate); 
                var nameToRead = Driver.FindElement(By.XPath(NameXpath)).Text;
                var widthToRead = Driver.FindElement(By.XPath(WidthXpath)).Text;
                Assert.IsTrue(nameToRead.Contains(nameToValidate));
                Assert.IsTrue(widthToRead.Contains(widthToValidate));

                Console.WriteLine("[INFO] Name: \"" + nameToValidate + "\", Width: \"" + widthToValidate + "\", Required: \"" + requiredToValidate + "\" validated successfully ");
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate Name: \"" + nameToValidate + "\", Width: \"" + widthToValidate + "\", Required: \"" + requiredToValidate + "\"---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClearTextByXpath
        public static void ClearTextByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                elementToTypeIn.Clear();
                Thread.Sleep(200);

                Console.WriteLine("[INFO] Cleared text by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to clear text by Xpath  ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClearTextAndPressEnterByXpath
        public static void ClearTextAndPressEnterByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                elementToTypeIn.Clear();
                SendKeys("ENTER");
                Thread.Sleep(200);
                Console.WriteLine("[INFO] Cleared text and Pressed ENTER by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to clear text and press ENTER by Xpath  ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Open New Tab
        #region OpenNewTab
        public static void OpenNewTab()
        {
            try
            {
                //Open Tab and then switch to tab
                ((IJavaScriptExecutor)SeleniumDriverUtility.Driver).ExecuteScript("window.open();");
                SeleniumDriverUtility.Driver.SwitchTo().Window(SeleniumDriverUtility.Driver.WindowHandles[1]);

                Console.WriteLine("[INFO] Opened new tab - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Open new tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Switch To Previous Tab
        #region SwitchToPreviousTab
        public static void SwitchToPreviousTab()
        {
            try
            {
                //Switch back to first tab
                SeleniumDriverUtility.Driver.SwitchTo().Window(SeleniumDriverUtility.Driver.WindowHandles[0]);

                Console.WriteLine("[INFO] Switched To Previous Tab - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Switch To Previous Tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpath0
        public static void ClickElementByXpath0(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                //Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[ERROR] Failed to click element by Xpath: " + elementXpath, e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpath2
        public static void ClickElementByXpath2(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to click on element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpath3
        public static void ClickElementByXpath3(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                //elementToClick.Click();
                Actions actions = new Actions(Driver);
                actions.MoveToElement(elementToClick).Click().Build().Perform();
                actions.MoveToElement(elementToClick).Click().Build().Perform();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                //Insert code for to display comment "(" Failed to click on element by Xpath - " + e.getMessage())"
                Console.WriteLine("\n[ERROR] Failed to click element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpath4
        public static void ClickElementByXpath4(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to click on element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpath5
        public static void ClickElementByXpath5(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                //elementToClick.Click();
                Actions actions = new Actions(Driver);
                actions.MoveToElement(elementToClick).Click().Build().Perform();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to click element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpath6
        public static void ClickElementByXpath6(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(1));
                Thread.Sleep(200);
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementToBeClickable(By.XPath(elementXpath)));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.InvisibilityOfElementLocated(By.Id("hellopreloader")));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                elementToClick.Click();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to click element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ClickElementByXpathUsingAction2
        public static void ClickElementByXpathUsingAction2(string elementXpath)
        {
            try
            {
                Console.WriteLine("[INFO] Clicking element by Xpath: " + elementXpath);
                WaitForElementByXpath(elementXpath);
                WebDriverWait wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
                wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.PresenceOfAllElementsLocatedBy(By.XPath(elementXpath)));
                var elementToClick = Driver.FindElement(By.XPath(elementXpath));
                //elementToClick.Click();
                //Thread.Sleep(3000);
                Actions actions = new Actions(Driver);
                actions.MoveToElement(elementToClick).Click().Perform();
                Console.WriteLine("[INFO] Clicked element by Xpath: " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to click element by Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ValidateEnteredText2ByXpath
        public string ValidateEnteredText2ByXpath(string elementXpath)
        {
            string retrievedText = "";
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath));
                retrievedText = elementToRead.Text; //.getText() in java
                Console.WriteLine("[INFO] Text: \"" + retrievedText + "\" retrieved successfully from element Xpath - " + elementXpath);

                return retrievedText;
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to retrieve text from element Xpath ---\n{0}", e.Message);
                return retrievedText;
            }
        }
        #endregion

        #region ValidateSelectedCheckbox2ByXpath
        public static void ValidateSelectedCheckbox2ByXpath(string elementXpath, string textToValidate)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath)).Text;
                Assert.IsTrue(elementToRead.Contains(textToValidate));
                Console.WriteLine("[INFO] Text: \"" + textToValidate + "\" validated successfully from element Xpath - " + elementXpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate text \"" + textToValidate + "\" from element Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }

        public static void ValidateSelectedCheckbox3ByXpath(string elementXpath, string checkboxToValidate)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToRead = Driver.FindElement(By.XPath(elementXpath)).GetAttribute("class");
                if (elementToRead.Contains("normalCheckBox  ng-valid ng-not-empty ng-dirty ng-valid-parse ng-touched"))
                {
                    Console.WriteLine("[INFO] Checkbox: \"" + checkboxToValidate + "\" is Checked and validated successfully from element Xpath - " + elementXpath);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate checkbox \"" + checkboxToValidate + "\" from element Xpath ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Open New Tab 2
        #region OpenNewTabByXpath
        public static void OpenNewTabByXpath(string elementXpath)
        {
            try
            {
                WaitForElementByXpath(elementXpath);
                var elementToTypeIn = Driver.FindElement(By.XPath(elementXpath));
                Actions action = new Actions(Driver);
                elementToTypeIn.Click();
                action.KeyDown(Keys.Control).SendKeys("T").KeyUp(Keys.Control).Perform(); //Ctrl+t
                Console.WriteLine("[INFO] Opened new tab - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Open new tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Open Url In New Tab
        #region OpenUrlInNewTab
        public static void OpenUrlInNewTab(string url)
        {
            try
            {
                // Define the script
                string script = "var d=document,a=d.createElement('a');a.target='_blank';a.href='" + url + "';a.innerHTML='.';d.body.appendChild(a);return a;";

                // Execute the JavaScript
                var element = (IWebElement)((IJavaScriptExecutor)Driver).ExecuteScript(script);

                // Click the new element
                element.Click();

                // Switch to the new tab
                Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                Console.WriteLine("[INFO] Opened new tab - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to Open new tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Switch To Tab Or Window
        #region SwitchToTabOrWindowByCss
        public static void SwitchToTabOrWindowByCss(string url)
        {
            try
            {
                Driver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + "t");
                //Driver.FindElement(By.TagName("body")).SendKeys(Keys.Control + "t"); 
                Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                Driver.Navigate().GoToUrl(url); //"http://www.google.com" for instance 
                //Global_NavigateToURL.GoToPrimaryURL("Host/Home");  

                /* //To Switch Back to Previous Window or Tab
                 * Driver.SwitchTo().Window(Driver.WindowHandles.First());
                 */
                //Driver.SwitchTo().Window(Driver.WindowHandles.First());
                Console.WriteLine("[INFO] Switched to window - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to switch to new tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Switch To Tab Or Window 2
        #region SwitchToTabOrWindow2ByCss
        public static void SwitchToTabOrWindow2ByCss(string url)
        {
            try
            {

                Actions action = new Actions(Driver);
                //Driver.FindElement(By.CssSelector("body")).SendKeys(Keys.Control + "t");
                //Driver.FindElement(By.TagName("body")).SendKeys(Keys.Control + "t" + Keys.Control);
                Driver.FindElement(By.CssSelector("body"));
                action.KeyDown(Keys.Control).SendKeys("t").KeyUp(Keys.Control).Perform(); //Ctrl+t
                Driver.SwitchTo().Window(Driver.WindowHandles.Last());
                Driver.Navigate().GoToUrl(url); //"http://www.google.com" for instance 
                //Global_NavigateToURL.GoToPrimaryURL("Host/Home");  

                /* //To Switch Back to Previous Window or Tab
                 * Driver.SwitchTo().Window(Driver.WindowHandles.First());
                 */
                //Driver.SwitchTo().Window(Driver.WindowHandles.First());
                Console.WriteLine("[INFO] Switched to window - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to switch to new tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        //Switch To Tabs
        #region SwitchToTabs
        public static void SwitchToTabs(int tabIndex) // tabIndex = 1
        {
            try
            {
                var windows = Driver.WindowHandles;
                Driver.SwitchTo().Window(windows[tabIndex]);
                Console.WriteLine("[INFO] Switched to tab - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[Error] Failed to switch to tab ---\n{0}", e.Message);
                Assert.Fail();
            }
        }
        #endregion

        #region ValidateTable
        public static void ValidateTable()
        { 
            try
            {
                //Grab the table
                var table = Driver.FindElement(By.XPath("//table/tbody"));

                //Now get all the TR elements from the table
                ReadOnlyCollection<IWebElement> allRows = table.FindElements(By.TagName("tr"));

                //And iterate over them, getting the cells             
                foreach (IWebElement row in allRows)
                {
                    ReadOnlyCollection<IWebElement> cells = row.FindElements(By.TagName("td"));
                    //Check the contents of each cell
                    foreach (IWebElement cell in cells)
                    {
                        if (cell.Text.Equals("User1"))
                        {
                            Console.WriteLine("********************************");
                            Console.WriteLine(cell.Text + " is in the list.");
                            Console.WriteLine("*********************");
                        }
                        else if (cell.Text.Equals("User2"))
                        {
                            Console.WriteLine("");
                            Console.WriteLine(cell.Text + " is in the list.");
                            Console.WriteLine("*********************");
                        } 
                    }
                }

                Console.WriteLine("[INFO] Validated Successfully that User1 and User2 are in the table - " + Driver.Title);
            }
            catch (Exception e)
            {
                Console.WriteLine("\n[ERROR] Failed to validate User(s) is(are) in the table ---\n{0}", e.Message);
                Assert.Fail();
            } 
        }
        #endregion 
    }
}

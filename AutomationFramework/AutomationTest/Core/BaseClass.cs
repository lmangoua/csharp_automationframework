﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutomationFramework.Entities;
using AutomationFramework;

/*
 * @author: lmangoua
 * Date: 20/08/2018
 * This Class is used for common methods over the project, to avoid repetition
 * 
 * */

namespace AutomationTest.Core
{
    public class BaseClass
    { 
        //StartDriver
        [TestInitialize]
        public void Init()
        {
            //To initialize the driver before it runs
            SeleniumDriverUtility.StartDriver();
        }

        //Close browser
        [TestCleanup]
        public void CleanUp()
        {
            SeleniumDriverUtility.Close();
        }
    }
}
